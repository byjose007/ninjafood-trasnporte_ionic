import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {controllerData} from '../../providers/controller';
import {modelData} from '../../providers/models';

@Component({
  templateUrl: 'build/pages/login/login.html',
  providers: [controllerData,modelData]
})
export class LoginPage {
  public IP : string = "172.17.118.220:8000";
  private api: controllerData;
  private apiModel: modelData;
  public positions;
  constructor(private nav: NavController, data: controllerData , model: modelData) {
    this.api = data;
    this.apiModel = model;
    this.api.IP = this.IP;
  }
  login(){
       //this.nav.push(IndexPage);     
  }
  getLocales(){
    this.api.getProveedores().subscribe(
         data => {
             this.positions = data.json();
             console.log(this.positions);
         },
         err => console.error(err),
         () => console.log('getRepos completed')
     );
  }
  cambiarIP()
  {
    this.apiModel.IP = this.IP;
    console.log('Ip cambiada');
  }  
}
