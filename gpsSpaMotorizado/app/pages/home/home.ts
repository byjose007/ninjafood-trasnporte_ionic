import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Geolocation} from 'ionic-native';
import {controllerData} from '../../providers/controller';
import {modelData} from '../../providers/models';

@Component({
  templateUrl: 'build/pages/home/home.html',
  providers: [controllerData,modelData]

})
export class HomePage {
  lon: number; 
  lat: number;
  IPServer: string;
  longitud: number; 
  latitud: number;
  anio: number = new Date().getFullYear();
  mes: number = new Date().getMonth();
  dia: number = new Date().getDate();
  hora: number = new Date().getHours();
  minuto: number = new Date().getMinutes();
  segundo: number = new Date().getSeconds();
  myDate: String = new Date().toISOString();
  date: Date ;
  public positions;
  private api: controllerData;
  private apiModel: modelData;
  constructor(private navCtrl: NavController, data: controllerData , model: modelData) {
    this.api = data;
    this.apiModel = model;
    this.date = new Date(this.anio,this.mes,this.dia,this.hora,this.minuto,this.segundo); 
    Geolocation.getCurrentPosition().then(pos => {
      this.lat = pos.coords.latitude;
      this.lon = pos.coords.longitude;
      this.IPServer = this.apiModel.IP;

      this.setCoordenadas(this.lon,this.lat,this.date,"");
      
    },function(error) {
        //this.err = 'x';             
    });
    let watch = Geolocation.watchPosition();
      watch.subscribe((data) => {
      this.anio  = new Date().getFullYear();
      this.mes  = new Date().getMonth();
      this.dia = new Date().getDate();
      this.hora  = new Date().getHours();
      this.minuto  = new Date().getMinutes();
      this.segundo  = new Date().getSeconds();
      this.date = new Date(this.anio,this.mes,this.dia,this.hora,this.minuto,this.segundo);
      this.latitud = data.coords.latitude;
      this.longitud = data.coords.longitude;
      this.IPServer = this.apiModel.IP;         
      this.setCoordenadas(this.longitud,this.latitud,this.date,"");
    });
    //this.getCoordenadas();    
  }

  getCoordenadas(){
    this.api.getCoordenadas("xx").subscribe(
         data => {
             this.positions = data.json();
             console.log(this.positions);
         },
         err => console.error(err),
         () => console.log('getRepos completed')
     );
  }
  setCoordenadas(longitud:number,latitud:number, date:Date, IP: string ){
    this.api.setCoordenadas(longitud,latitud,date,IP).subscribe(
         data => {
             this.positions = data.json();
             console.log(this.positions);
         },
         err => console.error(err),
         () => console.log('getRepos completed')
     );
  }
}
