
import {Injectable} from '@angular/core';
import {Jsonp} from '@angular/http';
import {Page,NavController, NavParams} from 'ionic-angular';
import {Modal, Alert} from 'ionic-angular';
import {URLSearchParams , Http , Response , Headers , RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class controllerData {
    public positions;
    public IP:string = "127.0.0.1:8000";
    constructor(private http: Http //, private data: LoginPage 
    ) {
        this.positions = null;
        //console.log(data.IP );
        
    }

    getProveedores() {
        let repos = this.http.get('http://127.0.0.1:8000/api/rest/coordenadas/?format=json');
        return repos;
    }

    getCoordenadas(IP:string) {    
        let repos = this.http.get('http://'+IP+'/api/rest/coordenadas/?format=json');        
        return repos;
    }      

    setCoordenadas(longitud:number , latitud: number, date:Date, IP:string ) {  
        let body = JSON.stringify({"longitud":longitud,"latitud":latitud, "fecha": date, "estado": "5", "id_perfil":3}); // JSON.stringify({ "longitud":longitud });
        //let headers = new Headers();
        let headers = new Headers({ 'Content-Type': 'application/json'});
        //, 'X-CSRF-TOKEN': this.getToken()});
        let options = new RequestOptions({ headers: headers });          
        let repos = this.http.post('http://'+this.IP+'/api/rest/coordenadas/', body, options);        
        return repos;
    } 
}