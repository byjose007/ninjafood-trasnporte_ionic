
import {Injectable} from '@angular/core';
import {Jsonp} from '@angular/http';
import {Page,NavController, NavParams} from 'ionic-angular';
import {Modal, Alert} from 'ionic-angular';
import {URLSearchParams , Http , Response , Headers , RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class modelData {    
    public IP:string = "172.17.118.220:8000";
    constructor(private http: Http //, private data: LoginPage 
    ) {        
    }
    getCoordenadas(IP:string) {    
        let repos = this.http.get('http://'+IP+'/api/rest/coordenadas/?format=json');        
        return repos;
    }      
}