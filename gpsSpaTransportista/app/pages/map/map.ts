
import {Component, Input, ViewChild, Renderer, Query, QueryList, ElementRef} from '@angular/core';
import {Geolocation, Device} from 'ionic-native';
import {IONIC_DIRECTIVES, NavParams} from 'ionic-angular';
import {controllerData} from '../../providers/controller';
import {TimerWrapper} from '@angular/core/src/facade/async';
import {URLSearchParams, Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
declare var ol: any;


@Component({
	selector: 'map-component',
	templateUrl: 'build/pages/map/map.html',
	directives: [IONIC_DIRECTIVES],
	providers: [controllerData]
})

export class MapComponent {
	lon: number;
	lat: number;
	pedidos: any[] = [];
	pedido: any;
	@ViewChild('map') map;

	constructor(public renderer: Renderer, public controllerData: controllerData,
		public params: NavParams,
		private http: Http) {
		this.pedido = this.params.get('pedido');
		if (this.pedido == undefined)
			return;
		Geolocation.getCurrentPosition().then(pos => {
			this.lat = pos.coords.latitude;
			this.lon = pos.coords.longitude;
			console.log("direccion actual, lat y log");
			console.log(this.lon + this.lat);
			this.ngAfterViewInitX();
			//console.log("tiempo de espera de 2000");
		}, function (error) {
			console.log(error);
		});
		//this.ngAfterViewInitX();
		/*TimerWrapper.setTimeout(() => {
			this.ngAfterViewInitX();
			console.log("tiempo de espera de 2000");
		}, 2000);*/
		//this.loadPedidos();
	}
	funcion() {
		Geolocation.getCurrentPosition().then(pos => {
			this.lat = pos.coords.latitude;
			this.lon = pos.coords.longitude;
		}, function (error) {

		});
	}
	SphericalCosinus(lat1: number, lon1: number, lat2: number, lon2: number) {
		var R = 6371; // km
		var dLon = this.toRad(lon2 - lon1),
			lat1 = this.toRad(lat1),
			lat2 = this.toRad(lat2),
			d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(dLon)) * R;
		return d * 1000;
	}
	toRad(x) {
		return x * Math.PI / 180;
	}
	ngAfterViewInitX() {

		var lon = this.lon;
		var lat = this.lat;
		if (this.lon == undefined) {
			this.lon = -79.20200;
			this.lat = -3.99714;
			console.log("ruta por defecto");
		}

		var points = [],
			msg_el = document.getElementById('msg'),
			url_osrm_nearest = 'http://router.project-osrm.org/nearest/v1/driving/',
			url_osrm_route = 'http://router.project-osrm.org/route/v1/driving/',
			/*icon_url_suc = 'image/sucursal1.png',
			icon_url_usr = 'image/cliente.png',
			icon_url_tra = 'image/transportista.png',*/
			icon_url_suc = 'image/icon-local.png',
			icon_url_usr = 'image/icon-client.png',
			icon_url_tra = 'image/icon-transport.png',
			vectorSource = new ol.source.Vector(),
			vectorLayer = new ol.layer.Vector({
				source: vectorSource
			}),
			stylesS = {
				route: new ol.style.Style({
					stroke: new ol.style.Stroke({
						width: 4, color: '#04B431'
					})
				}),
				icon: new ol.style.Style({
					image: new ol.style.Icon({
						anchor: [0.5, 0.5],
						src: icon_url_suc
					})
				})
			},
			stylesT = {
				route: new ol.style.Style({
					stroke: new ol.style.Stroke({
						width: 4, color: '#04B431'
					})
				}),
				icon: new ol.style.Style({
					image: new ol.style.Icon({
						anchor: [0.5, 0.5],
						src: icon_url_tra
					})
				})
			},
			styles = {
				route: new ol.style.Style({
					stroke: new ol.style.Stroke({
						width: 4, color: '#04B431'
					})
				}),
				icon: new ol.style.Style({
					image: new ol.style.Icon({
						anchor: [0.5, 0.5],
						src: icon_url_usr
					})
				})
			};
			console.log("estilos creados");
		var map = new ol.Map({
			target: 'map',
			layers: [
				new ol.layer.Tile({
					source: new ol.source.OSM()
				}),
				vectorLayer
			],
			view: new ol.View({
				center: ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:900913'),
				zoom: 15
			})
		});
		console.log("primera capa lista");
		var utils = {
			getNearest: function (coord) {
				var coord4326 = utils.to4326(coord);
				return new Promise(function (resolve, reject) {
					service.get(url_osrm_nearest + coord4326.join())
						.map(res => res.json())
						.subscribe(datap => {
							if (datap.code === 'Ok'){
								resolve(datap.waypoints[0].location);
							}
							else reject()
							//return datap;
						},
						error => {
							console.error("Error saving food! [ngAfterViewInitX]: "+error);
							return Observable.throw(error);
						});

				});
			},
			createFeature: function (coord, img) {
				var feature = new ol.Feature({
					type: 'place',
					geometry: new ol.geom.Point(ol.proj.fromLonLat(coord))
				});
				if (img == 1)
					feature.setStyle(styles.icon);
				else
					if (img == 2)
						feature.setStyle(stylesS.icon);
					else
						feature.setStyle(stylesT.icon);
				//vectorSource.clear()
				vectorSource.addFeature(feature);
			},
			createRoute: function (polyline, img) {
				// route is ol.geom.LineString
				var route = new ol.format.Polyline({
					factor: 1e5
				}).readGeometry(polyline, {
					dataProjection: 'EPSG:4326',
					featureProjection: 'EPSG:3857'
				});
				var feature = new ol.Feature({
					type: 'route',
					geometry: route
				});
				feature.setStyle(styles.route);
				vectorSource.addFeature(feature);
				points = [];
			},
			to4326: function (coord) {
				return ol.proj.transform([
					parseFloat(coord[0]), parseFloat(coord[1])
				], 'EPSG:3857', 'EPSG:4326');
			},
			to38576: function (coord) {
				return ol.proj.transform([
					parseFloat(coord[0]), parseFloat(coord[1])
				], 'EPSG:4326', 'EPSG:900913');
			}
		};
		console.log("transformando cordenadas");
		let service: any = this.http;

		function routeMap(pos_actual, img) {
			pos_actual = utils.to38576(pos_actual);
			utils.getNearest(pos_actual).then(function (coord_street) {
				var last_point = points[points.length - 1];
				var points_length = points.push(coord_street);
				utils.createFeature(coord_street, img);
				var coord6 = utils.to38576(coord_street);
				if (points_length < 2) {
					return;
				}

				//get the route
				var point1 = last_point.join();
				var point2 = coord_street[0] + "," + coord_street[1];

				service.get(url_osrm_route + point1 + ';' + point2)
					.map(res => res.json())
					.subscribe(datap => {
						if (datap.code !== 'Ok') {
							msg_el.innerHTML = 'No route found.';
							return;
						}
						utils.createRoute(datap.routes[0].geometry, img);
					},
					error => {
						console.error("Error saving food! [routeMap]");
						return Observable.throw(error);
					});


			});
			console.log("creando puntos de enlace del uno al otro");
		}


		/*var center = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
		var lonCenter = center[0];
		var latCenter = center[1];

		//zoomend
		//moveend
		map.on("moveend", function(e){
			var dat = ol.proj.transform(e.map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
			var coord_street = [dat[0], dat[1]];
			utils.createFeature(coord_street, 1);
		});*/		

		//routeMap({ 0: -8817259.633037006, 1: -444218.5671935696 }, 3);
		//routeMap({ 0: -8815259.643037006, 1: -444418.5271935696 }, 3);
		var longitude = lon;
		var latitude = lat;
		//var coord_street = [lon, lat];
		routeMap({ 0: Number(this.lon), 1: Number(this.lat) }, 3);
		//utils.createFeature(coord_street, 3);		
		TimerWrapper.setTimeout(() => {
			//longitude = this.pedido.id_sucursal.id_direccion.longitud;
			//latitude = this.pedido.id_sucursal.id_direccion.latitud;			
			routeMap({ 0: Number(this.pedido.id_sucursal.id_direccion.longitud), 1: Number(this.pedido.id_sucursal.id_direccion.latitud) }, 2);
			TimerWrapper.setTimeout(() => {
				points = [];
				routeMap({ 0: Number(this.pedido.id_sucursal.id_direccion.longitud), 1: Number(this.pedido.id_sucursal.id_direccion.latitud) }, 2);
				TimerWrapper.setTimeout(() => {
					//longitude = this.pedido.id_direccion.longitud;
					//latitude = this.pedido.id_direccion.latitud;
					routeMap({ 0: Number(this.pedido.id_direccion.longitud), 1: Number(this.pedido.id_direccion.latitud) }, 1);
					//utils.createFeature(coord_street, 1);
				}, 1000);
			}, 1000);
		}, 1000);
	}
}