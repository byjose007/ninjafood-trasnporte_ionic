import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import {UbicacionPage} from '../ubicacion/ubicacion';
import {Observable} from "rxjs/Rx";
import {controllerData} from '../../providers/controller';
import {TimerWrapper} from '@angular/core/src/facade/async';
import { InicioPage } from '../inicio/inicio';
@Component({
  templateUrl: 'build/pages/detalle-pedido/detalle-pedido.html',
  providers: [controllerData]
})
export class DetallePedidoPage {
  pedido : any;
  constructor(private navCtrl: NavController, public params: NavParams, private viewCtrl: ViewController,
              private alertCtrl: AlertController, public controllerData: controllerData) {
    this.pedido = this.params.get('pedido');
  } 
  closeModal()
  {
    this.viewCtrl.dismiss();
  } 
  showConfirmPedido(pedido) {
    let confirm = this.alertCtrl.create({
      title: 'Confirmar Entrega?',
      message: 'Confirmas la entrega de este pedido a ?',
      inputs: [
        {
          name: 'tiempo',
          type: 'number',
          placeholder: 'Tiempo de entrega'
        },
      ],      
      buttons: [
        {
          text: 'Si',
          handler: data => {
            if(data.tiempo == "")
            {
              this.mansajeAlert("Error","Ingresa el tiempo estimado de entrega");
              return;
            }else
            {
              pedido.tiempo += parseInt(data.tiempo);
              this.confirmarEntrega(pedido);                            
              //this.navCtrl.pop();                            
              //this.closeModal();
            }
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('No');
          }
        }
      ]
    });
    confirm.present();
  } 
  mansajeAlert(titulo, texto) {
    let confirm = this.alertCtrl.create({
      title: titulo,
      message: texto
    });
    confirm.present();
    TimerWrapper.setTimeout(() => {
      confirm.dismiss();
    }, 3000);
  }   
  verUbicacion(pedido: any) {
    this.navCtrl.push(UbicacionPage, {
      pedido: pedido
    });
  } 
  confirmarEntrega(pedido) {
    this.controllerData.setEntrega(pedido, 1).subscribe(
      data => {      
        pedido.id_estado.id_estado = 3;  
        return true;
      },
      error => {
        console.error("Error saving food! [confirmarEntrega]");
        return Observable.throw(error);
      }
    );
  }   
  pedidoEntregado(pedido,estado) {
    this.controllerData.setEntrega(pedido, estado).subscribe(
      data => {
        this.closeModal();
        return true;
      },
      error => {
        console.error("Error saving food! [pedidoEntregado]");
        return Observable.throw(error);
      }
    );
  }        

  goBack() {
    this.navCtrl.pop();
  }
}
