import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import {Geolocation, Device} from 'ionic-native';
import {controllerData} from '../../providers/controller';
import {RegistroModel} from '../../providers/registro-model';
import {UsuarioModel} from '../../providers/usuario-model';
import {Observable} from "rxjs/Rx";
import {TimerWrapper} from '@angular/core/src/facade/async';
@Component({
  templateUrl: 'build/pages/saldo/saldo.html',
  providers: [controllerData]
})
export class SaldoPage {

  sim_dispositivo: any = "";
  saldo : number = 0.0;
  
  constructor(public navCtrl: NavController, public controllerData: controllerData,
    private alertCtrl: AlertController) {
    if (Device.device.serial == undefined) {
      this.sim_dispositivo = '12345';
    }
    else
      this.sim_dispositivo = Device.device.serial;
  }
  cargaSaldo() {
    this.controllerData.getDispositivo(this.sim_dispositivo, 1).then(
      data => {
        if (data == 0)
          this.mansajeAlert('Error al ingresar', 'Usuario o clave incorrectos!!!');
      },
      error => {
        this.mansajeAlert('Error Desconocido', 'Comuniquese con el administrador del sistema!!!');
        return Observable.throw(error);
      }
    );
  }
  mansajeAlert(titulo, texto) {
    let confirm = this.alertCtrl.create({
      title: titulo,
      message: texto
    });
    confirm.present();
    TimerWrapper.setTimeout(() => {
      confirm.dismiss();
    }, 5000);
  }
  elementChanged(numero) {
    /*if (numero.length < 10) {
      this.desabilitado = true;
    }
    if (numero.length >= 10) {
      this.desabilitado = false;
      this.registro.numero = numero.substring(0, 10);
    }*/
  }

  getBackgroundSaldo(){
    if(this.saldo > 0)
      return '#17B890';    
    else
      return '#E4022D';
  }

  goBack() {
    //et n = this.navCtrl.length();
    this.navCtrl.pop();
    ////this.navCtrl.remove(n);
  }
}
