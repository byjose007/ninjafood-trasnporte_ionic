import { Component } from '@angular/core';
import { InicioPage } from '../inicio/inicio';
import { UbicacionPage } from '../ubicacion/ubicacion';
import { RegistroPage } from '../registro/registro';
import { SaldoPage } from '../saldo/saldo';
@Component({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {

  public tab1Root: any;
  public tab2Root: any;
  public tab3Root: any;

  constructor() {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    this.tab1Root = InicioPage;
    this.tab2Root = SaldoPage;
    this.tab3Root = RegistroPage;
  }
}
