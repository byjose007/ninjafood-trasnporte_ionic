import { Component } from '@angular/core';
import { Platform, ionicBootstrap, Storage, LocalStorage, NavController } from 'ionic-angular';
import { StatusBar, SQLite } from 'ionic-native';
import { TabsPage } from './pages/tabs/tabs';
import { SlidesPage } from './pages/slides/slides';
import { InicioPage } from './pages/inicio/inicio';
import { RegistroPage } from './pages/registro/registro';
import { BackgroundMode, LocalNotifications } from 'ionic-native';
import { TimerWrapper } from '@angular/core/src/facade/async';

declare var cordova: any;
@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>'
})
export class MyApp {

  public rootPage: any;

  constructor(private platform: Platform) {

    this.startPage().then(res => {
      console.log("res!!! " + res);
      if (!res) {
        this.rootPage = SlidesPage;
      } else {
        if (res == '0') {
          this.rootPage = RegistroPage;
        } else
          this.rootPage = InicioPage;
        //this.rootPage = SlidesPage;
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //console.log(Device.device.cordova);
      StatusBar.styleDefault();

      document.addEventListener('deviceready', function () {
        // cordova.plugins.backgroundMode is now available
        //cordova.plugins.backgroundMode.setDefaults({ text:'Doing heavy tasks.l.'});
        console.log("isEnabled: " + cordova.plugins.backgroundMode.isEnabled());
        console.log("isActive: " + cordova.plugins.backgroundMode.isActive());
        cordova.plugins.backgroundMode.enable();
        console.log("isEnabled: " + cordova.plugins.backgroundMode.isEnabled());
        console.log("isActive: " + cordova.plugins.backgroundMode.isActive());
        cordova.plugins.backgroundMode.onactivate = function () {
          /*TimerWrapper.setInterval(() => {
            console.log("isActive: "+cordova.plugins.backgroundMode.isActive());
            LocalNotifications.schedule({
            id: 1,
            title: 'Segundo Plano by Milton',
            led: '6699ff',
            sound: 'file://sounds/capisci_a.mp3'
          });
    }, 9000);*/
        }
      }, false);
    });
  }

  /*enviarNotificacion(title: string) {
    LocalNotifications.schedule({
      id: 1,
      title: title,
      //text: 'El restaurante a confirmado tu pedido',
      led: '6699ff',
      sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
      //at: new Date(new Date().getTime() + 3600),                
      //data: { secret: key }
    });
  }*/

  startPage() {
    return new Promise(resolve => {
      let db = new SQLite();
      let sqlQuerySesion = 'SELECT token, registro FROM startApp';
      //let sqlQuerySesion = 'SELECT count(*) FROM startApp';  
      db.openDatabase({
        name: 'tgoodappetit.db',
        location: 'default' // the location field is required
      }).then(base => {
        db.executeSql(sqlQuerySesion, []).then((rta) => {
          console.log("rta!!! Login1: " + rta.rows.length);
          if (rta.rows.length != 0) {
            //console.log("fuck yea!!! Login1");
            resolve(rta.rows.item(0).registro);
          } else {
            //console.log("fuck yea!!! SLide1");
            resolve(rta.rows.item(0).registro);
          }
        }, (err) => {
          console.log("fuck yea!!! SLIDER2");
          console.error(JSON.stringify(err));
          resolve(false);
        })
      }, (err) => {
        console.error(JSON.stringify(err));
        console.log("fuck yea!!! Login2" + err);
        resolve(false);
      });
      console.log("fuck yea!!! SLide3");
      return (true);
    });
  }

}

ionicBootstrap(MyApp);
