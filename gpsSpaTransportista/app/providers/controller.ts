
import {Injectable} from '@angular/core';
import {Jsonp} from '@angular/http';
import {Page, NavController, NavParams, Platform} from 'ionic-angular';
import {Device} from 'ionic-native';
import {Modal, Alert} from 'ionic-angular';
import {URLSearchParams, Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {UsuarioModel} from '../providers/usuario-model';
import { CustomValidations } from '../validations/CustomValidations';
@Injectable()
export class controllerData extends CustomValidations {
    public positions;
    data: any[] = [];
    user: any;
    perfil: any;
    pedido: any;
    registroExistente: any = {};
    error: any;
    transportista: any = "";
    //public IP: string = "https://www.ajamar.com";
    public IP: string = 'https://goodappetit.com';
    //public IP: string = 'http://172.18.250.138:8000';
    //public IP: string = "http://172.17.113.28:9000";
    //public IP: string = "http://localhost:8000";

    constructor(private http: Http , public params: NavParams,
        protected navCtrl: NavController,
        protected platform: Platform) {
        super(navCtrl, platform);
        this.positions = null;
        this.getPedido();
        if (Device.device.serial == undefined) {
            //this.registroExistente.sim_dispositivo = "420395b7de4ab100";
            this.registroExistente.sim_dispositivo = '12345';
        }
        else
            this.registroExistente.sim_dispositivo = Device.device.serial;
        
        this.getTransportista(this.registroExistente)
            .then(data => {
                if (data)
                    this.registroExistente = data;
            });
    }

    getProveedores() {
        let repos = this.http.get(this.IP + '/api/rest/coordenadas/?format=json');
        return repos;
    }

    getCoordenadas(IP: string) {
        let repos = this.http.get(this.IP + '/api/rest/coordenadas/?format=json');
        return repos;
    }

    getPedidos() {
        this.data = null;
        if (this.data) {
            return Promise.resolve(this.data);
        }
        return new Promise(resolve => {
            this.data = [];
            this.http.get(this.IP + '/api/rest/pedido/')
                .map(res => res.json(), err => this.checkServerResponse(err))
                .subscribe(datap => {
                    this.data = datap;
                    //resolve(this.data);
                    this.getUsuario(this.user, this.registroExistente);
                    if(this.registroExistente.id_transportista!=undefined)
                    this.http.get(this.IP + '/api/rest/PedidoEntregado/' + this.registroExistente.id_transportista + '/')
                    .map(res => res.json(), err => this.checkServerResponse(err))
                    .subscribe(dataE => {
                        if (dataE.length > 0) {
                            dataE.forEach(p => this.data.push(p));
                        }
                        resolve(this.data);
                    },
                    error => {
                        console.error("Error al obtener pedidos en general! "+this.registroExistente);
                        return Observable.throw(error);
                    });
                },
                error => {
                    console.error("Error saving food! [getPedidos]");
                    return Observable.throw(error);
                });

        });
    }
    getPedidosEntregados() {
        this.data = null;
        if (this.data) {
            return Promise.resolve(this.data);
        }
        return new Promise(resolve => {
            this.http.get(this.IP + '/api/rest/PedidoEntregado/' + this.registroExistente.id_transportista + '/')
                .map(res => res.json(), err => this.checkServerResponse(err))
                .subscribe(data => {
                    this.data = data;
                    resolve(this.data);
                },
                error => {
                    console.error("Error saving food! [getPedidosEntregados]");
                    return Observable.throw(error);
                });
        });
    }
    getTransportista(registro) {
        return new Promise(resolve => {
            this.http.get(this.IP + '/api/rest/transportistaExistente/' + registro.sim_dispositivo +
                '/' + registro.numero + '/')
                .map(res => res.json(), err => this.checkServerResponse(err))
                .subscribe(data => {
                    this.registroExistente = data[0];
                    resolve(this.registroExistente);
                });
        });
    }
    getUsuario(usuario, registro) {
        if (usuario == undefined) {
            this.http.get(this.IP + '/api/rest/transportistaExistente/' + registro.sim_dispositivo +
                '/' + registro.numero + '/')
                .map(res => res.json(), err => this.checkServerResponse(err))
                .subscribe(data => {
                    if (data.length == 0) {
                        this.setRegistro(registro);
                        //resolve(2);
                    }
                    else {
                        this.registroExistente = data[0];
                        //resolve(1);
                        //resolve(this.registroExistente);
                    }
                },
                error => {
                    console.error("Error al obtener el transportista existente!=> "+registro.sim_dispositivo);
                    return Observable.throw(error);
                });
            return;
        }
        this.user = null;
        if (this.user) {
            return Promise.resolve(this.user);
        }
        return new Promise(resolve => {
            this.http.get(this.IP + '/api/rest/usuario/' + usuario.usuario + '/')
                .map(res => res.json(), err => this.checkServerResponse(err))
                .subscribe(data => {
                    this.user = data;
                    if (this.user.length == 0) {
                        resolve(0);
                        return;
                    }
                    else
                        this.http.get(this.IP + '/api/rest/perfil/' + this.user[0].id + '/')
                            .map(res => res.json(), err => this.checkServerResponse(err))
                            .subscribe(dataP => {
                                if (dataP.length > 0) {
                                    this.perfil = dataP[0];
                                    registro.id_perfil = dataP[0].id_perfil;

                                    this.http.get(this.IP + '/api/rest/transportistaExistente/' + registro.sim_dispositivo +
                                        '/' + registro.numero + '/')
                                        .map(res => res.json())
                                        .subscribe(data => {
                                            if (data.length == 0) {
                                                this.setRegistro(registro);
                                                resolve(2);
                                            }
                                            else {
                                                this.registroExistente = data[0];
                                                resolve(this.perfil);
                                            }
                                        },
                                        error => {
                                            console.error("Error al obtener el perfil del transportista!");
                                            return Observable.throw(error);
                                        });


                                }
                            },
                            error => {
                                console.error("Error al obtener el perfil!");
                                return Observable.throw(error);
                            });
                },
                error => {
                    console.error("Error al obtener el perfil usuario=>"+ usuario.usuario +" sim=>"+ registro.sim_dispositivo);
                    return Observable.throw(error); 
                });
        });
    }

    getPedido() {
        if (this.pedido) {
            return Promise.resolve(this.pedido);
        }
        return new Promise(resolve => {
            this.http.get(this.IP + '/api/rest/pedidoEstado/')
                .map(res => res.json(), err => this.checkServerResponse(err))
                .subscribe(data => {
                    this.pedido = data;
                    resolve(this.data);
                });
        });
    }

    getDispositivo(sim_dispositivo, saldo) {
        this.http.get(this.IP + '/api/rest/transportistaExistente/' + sim_dispositivo + '/0/')
            .map(res => res.json(), err => this.checkServerResponse(err))
            .subscribe(data => {
                if (data.length == 0) {
                    //this.transportista = data;
                    //this.setRegistro(saldo);
                    //resolve(2);
                }
                else {
                    this.registroExistente = data[0];
                    this.registroExistente.saldo += saldo;
                    this.setSaldo(this.registroExistente);
                    //resolve(this.registroExistente);
                }
            },
            error => {
                console.error("Error al obtener el dispositivo!");
                return Observable.throw(error);
            });
        return new Promise(resolve => {
            this.setSaldo(this.registroExistente);
            resolve(1);
        });

    }

    setCoordenadas(longitud: number, latitud: number, date: Date, IP: string) {
        let body = JSON.stringify({ "longitud": longitud, "latitud": latitud, "fecha": date, "estado": "5", "id_perfil": 3 }); // JSON.stringify({ "longitud":longitud });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let repos = this.http.post(this.IP + '/api/rest/coordenadas/', body, options);
        return repos;
    }
    setEntrega(pedido: any, idTransportista: number) {
        let pedido_nuevo = this.pedido.filter(x => x.id_pedido == pedido.id_pedido)[0];
        if (pedido_nuevo.id_estado == 2)
            pedido_nuevo.id_estado = 3;
        else
            if (pedido_nuevo.id_estado == 4)
                pedido_nuevo.id_estado = 9;
            else
                pedido_nuevo.id_estado = 5;
        pedido_nuevo.id_trasportista = this.registroExistente.id_transportista;
        pedido_nuevo.tiempo = pedido.tiempo;
        let body = JSON.stringify(pedido_nuevo);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let repos = this.http.put(this.IP + '/api/rest/pedidoEstado/' + pedido.id_pedido + '/', body, options).map((res: Response) => res.json());
        return repos;
    }
    setSaldo(registro: any): boolean {
        let body = JSON.stringify(registro);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let repos = this.http.put(this.IP + '/api/rest/transportista/' + registro.id_trasportista, body, options)
            .map(res => res.json(), err => this.checkServerResponse(err))
            .subscribe(data => {
                this.error = data;
                return true;
            },
            error => {
                console.error("Error al guardar el saldo!");
                return Observable.throw(error);
            });
        return false;
    }
    setRegistro(registro: any): boolean {
        let body = JSON.stringify(registro);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let repos = this.http.post(this.IP + '/api/rest/transportista/', body, options)
            .map(res => res.json(), err => this.checkServerResponse(err))
            .subscribe(data => {
                this.error = data;
                return true;
            },
            error => {
                console.error("Error al guardar el registro!");
                return Observable.throw(error);
            });
        return false;
    }
}