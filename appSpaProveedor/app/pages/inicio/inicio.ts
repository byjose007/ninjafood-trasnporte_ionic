import {Page, NavController, Alert, ViewController, ModalController, NavParams, AlertController, Platform } from 'ionic-angular';
import {Query, QueryList, Component, ElementRef} from '@angular/core';
import {controllerData} from '../../providers/controller';
//import {UbicacionPage} from '../ubicacion/ubicacion';
import {DetallePedidoPage} from '../detalle-pedido/detalle-pedido';
//import {MapComponent} from '../map/map';
import {TimerWrapper} from '@angular/core/src/facade/async';
import {Geolocation, Device, LocalNotifications} from 'ionic-native';
import {Observable} from "rxjs/Rx";
import { RegistroPage } from '../../pages/registro/registro';
//import { SaldoPage } from '../../pages/saldo/saldo';
import { CustomValidations } from '../../validations/CustomValidations';
//import { BackgroundMode } from 'ionic-native';

//import {LoginPage} from '../inicio/inicio';

declare var ol: any;
//BackgroundMode.enable();
@Component({
  templateUrl: 'build/pages/inicio/inicio.html',
  //directives: [MapComponent],
  providers: [controllerData]
})
export class InicioPage extends CustomValidations {
  anio: number = new Date().getFullYear();
  mes: number = new Date().getMonth();
  dia: number = new Date().getDate();
  hora: number = new Date().getHours();
  minuto: number = new Date().getMinutes();
  segundo: number = new Date().getSeconds();
  myDate: String = new Date().toISOString();
  tiempoEstimado: number;
  date: Date;
  pedidos: any[] = [];
  nuevo_pedido: any = {};
  lon: number;
  lat: number;
  cronometro: number = 60;
  calle_principal: String;
  calle_secundaria: String;
  numero_casa: String;
  referencia: String;
  distancia: number = 2000;
  perfil: any;
  txtError: String;
  constructor(public navCtrl: NavController, public controllerData: controllerData,
    private viewCtrl: ViewController, private modalCtrl: ModalController,
    public params: NavParams,
    public alertCtrl: AlertController,
    protected platform: Platform/*,
    public backgroudMode: BackgroundMode*/) {

    //BackgroundMode.enable();

    super(navCtrl, platform);
    this.perfil = this.params.get('perfil');
    this.date = new Date(this.anio, this.mes, this.dia, this.hora, this.minuto, this.segundo);
    var time = new Date();
    this.loadPedidos();
    TimerWrapper.setInterval(() => {
      //this.enviarNotificacion("Tienes un nuevo pedido de");
      this.loadPedidos();
    }, 30000);
    TimerWrapper.setInterval(() => {
      this.funcion();
    }, 30000);
  }
  loadPedidos() {
    this.checkNetwork();
    this.controllerData.getPedidos()
      .then(data => {
        if (data != null && data.length > 0) {
          this.txtError = null;
          let nuevo = true;
          data.forEach(element => {
            if (this.pedidos.length > 0) {
              this.pedidos.forEach(p => {
                if (p.id_pedido == element.id_pedido && p.id_estado.id_estado == 2)
                  nuevo = false;
              });
            }
          });
          if (nuevo) {
            //this.nuevo_pedido = element;
            this.enviarNotificacion("Tienes un nuevo pedido");// de '"+ element.id_sucursal.nombre_sucursal+"'");
          }
          this.pedidos = data;
          if (this.pedidos.length > 0)
            this.pedidos.forEach(p => p.id_direccion.id_perfil.ruta_foto.split('http').length > 2 ?
              p.id_direccion.id_perfil.ruta_foto = "http" + decodeURIComponent(p.id_direccion.id_perfil.ruta_foto.split('http')[2])
              : p.id_direccion.id_perfil.ruta_foto);
        }
        else {
          this.pedidos = null;
          this.txtError = "Aun no se han realizado pedidos.";
        }
      });
  }
  loadPedidosEntregados() {
    this.controllerData.getPedidosEntregados()
      .then(data => {
        //this.pedidos = data;
        data.forEach(p => this.pedidos.push(p));
      });
  }
  funcion() {
    //this.time = new Date();
    Geolocation.getCurrentPosition().then(pos => {
      this.lat = pos.coords.latitude;
      this.lon = pos.coords.longitude;
      var longitud = -79.210953;
      var latitud = -4.0114529;
      for (var posicion in this.pedidos) {
        var distance = this.SphericalCosinus(this.lon, this.lat,
          this.pedidos[posicion].id_direccion.longitud, this.pedidos[posicion].id_direccion.latitud);
        if (distance < this.distancia) {
          /*LocalNotifications.schedule({
            id: 1,
            text: 'Tienes una nueva entrega',
            sound: null,
            data: { secret: 1 }
          });*/
          /*this.calle_principal = this.pedidos[posicion].id_direccion.calle_principal;
          this.calle_secundaria = this.pedidos[posicion].id_direccion.calle_secundaria;
          this.numero_casa = this.pedidos[posicion].id_direccion.numero_casa;
          this.referencia = this.pedidos[posicion].id_direccion.referencia;*/
        }
      }
    }, function (error) {
    });
  }

  enviarNotificacion(title: string) {
    LocalNotifications.schedule({
      id: 1,
      title: title,
      //text: 'El restaurante a confirmado tu pedido',
      led: '6699ff',
      sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
      //at: new Date(new Date().getTime() + 3600),                
      //data: { secret: key }
    });
  }

  toRad(x) {
    return x * Math.PI / 180;
  }

  SphericalCosinus(lat1: number, lon1: number, lat2: number, lon2: number) {
    var R = 6371; // km
    var dLon = this.toRad(lon2 - lon1),
      lat1 = this.toRad(lat1),
      lat2 = this.toRad(lat2),
      d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(dLon)) * R;
    return d * 1000;
  }
  confirmarEntrega(pedido) {
    //alert(this.tiempoEstimado);
    //this.controllerData.setEntrega(1,1,this.date); 
    this.controllerData.setEntrega(pedido, 1).subscribe(
      data => {
        // refresh the list
        this.loadPedidos();
        return true;
      },
      error => {
        console.error("Error saving food!");
        return Observable.throw(error);
      }
    );
  }

  itemSelected(pedido: any) {
    /*this.navCtrl.push(UbicacionPage, {
      pedido: pedido
    });*/
    let modal = this.modalCtrl.create(DetallePedidoPage, {
      pedido: pedido
    });
    modal.onDidDismiss(data => {
      this.pedidos = null;
      this.loadPedidos();
    });
    modal.present();
  }

  verDatelles(pedido: any) {
    alert(pedido.id_direccion.calle_principal);
  }

  public salir() {
    this.navCtrl.setRoot(RegistroPage);
  }

  public goToSaldo() {
    //this.navCtrl.push(SaldoPage);
  }

  ionViewWillEnter() {
    this.LocationValidator();
  }

  /*LocationValidator(){
      //Geolocation.
      if ((<any> window).cordova) {
        (<any> window).cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
          if(enabled == false){
            alert("Geolocalizacion Deshabilitada");
            console.log("Antes presentAlertGeo");
            //showAlert();
            console.log("Despues presentAlertGeo");
            //this.presentAlertGeo("Geolocalizacion Deshabilitada", "Geolocalización");
            (<any> window).cordova.plugins.diagnostic.switchToLocationSettings();
          } 

        }, function(error) {
            //alert("The following error occurred: " + error);
            console.log("The following error occurred: " + error);
        });
      }
    }*/

  LocationValidator() {
    //Geolocation.
    if ((<any>window).cordova) {
      (<any>window).cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
        if (enabled == false) {
          alert("Geolocalizacion Deshabilitada");
          console.log("Antes presentAlertGeo");
          //showAlert();
          console.log("Despues presentAlertGeo");
          //this.presentAlertGeo("Geolocalizacion Deshabilitada", "Geolocalización");
          (<any>window).cordova.plugins.diagnostic.switchToLocationSettings();
        }

      }, function (error) {
        //alert("The following error occurred: " + error);
        console.log("The following error occurred: " + error);
      });
    }
  }

  public showAlert() {
    console.log("Dentro del Alert presentAlertGeo 1");
    let alert = this.alertCtrl.create({
      title: 'Geolocalizacion Deshabilitada',
      subTitle: 'Por favor, habilitar la Geolocalizacion',
      buttons: ['OK']
    });
    console.log("Dentro del Alert presentAlertGeo 2");
    alert.present();
    console.log("Dentro del Alert presentAlertGeo 3");
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
}

