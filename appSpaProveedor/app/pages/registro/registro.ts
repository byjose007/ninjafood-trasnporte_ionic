import { Component } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import {Geolocation, Device} from 'ionic-native';
import {controllerData} from '../../providers/controller';
import {RegistroModel} from '../../providers/registro-model';
import {UsuarioModel} from '../../providers/usuario-model';
import {Observable} from "rxjs/Rx";
import {TimerWrapper} from '@angular/core/src/facade/async';
import { InicioPage } from '../../pages/inicio/inicio';
import { CustomValidations } from '../../validations/CustomValidations';
//import { BackgroundMode, LocalNotifications } from 'ionic-native';

//declare var cordova:any;
@Component({
  templateUrl: 'build/pages/registro/registro.html',
  providers: [controllerData]
})
export class RegistroPage extends CustomValidations {
  desabilitado: boolean = true;
  usuario: UsuarioModel;
  registro: RegistroModel;
  tipos: any[]=["Taxi","Moto","Bicicleta","Persona"]; 
  constructor(public navCtrl: NavController, public controllerData: controllerData,
    private alertCtrl: AlertController,
    protected platform: Platform) {

    super(navCtrl, platform);
    /*cordova.plugins.backgroundMode.enable();
    cordova.plugins.backgroundMode.onactivate = function () {
      
    };
    BackgroundMode.enable();
    BackgroundMode.isActive().then(x => {
      this.enviarNotificacion("isActive");
    });
    BackgroundMode.isEnabled().then(x => {
      this.enviarNotificacion("isEnabled");
    });
    TimerWrapper.setInterval(() => {
        this.enviarNotificacion("Tienes un nuevo pedido de");
      }, 5000);*/
    this.registro = new RegistroModel("", 0, "", "", "", "",0);
    this.usuario = new UsuarioModel("", "", "");

    this.registro.id_perfil = 48;
    this.registro.tipo_servicio = "entrega";    
    this.usuario.usuario = '';
    this.usuario.clave = '';

    if (Device.device.serial == undefined) {
      this.registro.sim_dispositivo = '12345';
      //this.registro.sim_dispositivo = "420395b7de4ab100";
    }
    else
      this.registro.sim_dispositivo = Device.device.serial;
  }

  /*enviarNotificacion(title: string) {
    LocalNotifications.schedule({
      id: 1,
      title: title,
      //text: 'El restaurante a confirmado tu pedido',
      led: '6699ff',
      sound: this.platform.is('android') ? 'file://sounds/capisci_a.mp3' : 'file://sounds/capisci_ios.m4r'
      //at: new Date(new Date().getTime() + 3600),                
      //data: { secret: key }
    });
  }*/

  registrarDispositivo() {

    this.checkNetwork();
    if (
      (this.usuario.usuario == "" || this.usuario.usuario == undefined) ||
      (this.usuario.clave == "" || this.usuario.clave == undefined)
    ) {
      this.mansajeAlert('Informacion Incompleta', 'Ingrese los datos necesarios!!!');
      return;
    }
    this.usuario = new UsuarioModel(this.usuario.usuario, this.usuario.clave, this.registro.numero);
    this.controllerData.getUsuario(this.usuario, this.registro).then(
      data => {
        if (data == 0)
          this.mansajeAlert('Error al ingresar', 'Usuario o clave incorrectos!!!');
        else
          if (data == 2){
            this.mansajeAlert('Registro Correcto', 'El registro se ha realizado correctamente!!!');
            this.navCtrl.setRoot( InicioPage );            
          }
          else{
            //this.mansajeAlert('Registro Correcto', 'El registro se ha realizado correctamente!!!');
            this.navCtrl.setRoot( InicioPage, {perfil: data} );
          }
        return true;
      },
      error => {
        this.mansajeAlert('Error Desconocido', 'Comuniquese con el administrador del sistema!!!');
        return Observable.throw(error);
      }
    );
  }
  mansajeAlert(titulo, texto) {
    let confirm = this.alertCtrl.create({
      title: titulo,
      message: texto
    });
    confirm.present();
    TimerWrapper.setTimeout(() => {
      confirm.dismiss();
    }, 5000);
  }
  elementChanged(numero) {
    if (numero.length < 10) {
      this.desabilitado = true;
    }
    if (numero.length >= 10) {
      this.desabilitado = false;
      this.registro.numero = numero.substring(0, 10);
    }
  }
}
