import {NavController, Platform} from 'ionic-angular';
import {Network} from 'ionic-native';

declare var window: any;

export class CustomValidations {

    constructor(
        protected navCtrl: NavController,
        protected platform: Platform
    ) {}

    //JY, Validar conexion a la red
   checkNetwork() {
        this.platform.ready().then(() => {
            /*var networkState = navigator.connection.type;
            var states = {};
            states[Connection.UNKNOWN]  = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI]     = 'WiFi connection';
            states[Connection.CELL_2G]  = 'Cell 2G connection';
            states[Connection.CELL_3G]  = 'Cell 3G connection';
            states[Connection.CELL_4G]  = 'Cell 4G connection';
            states[Connection.CELL]     = 'Cell generic connection';
            states[Connection.NONE]     = 'No network connection';*/
            
            console.log(<string> Network.connection);
            if(<string> Network.connection == "none"){
              this.showToast("Error de Red, Por favor verifique su conexión", "long", "bottom");
            }
        });
    }

    //JY, Validar conexion al Servidor (API Rest)
   checkServerResponse(err) {
        this.platform.ready().then(() => {
            
            console.log(err);
            this.showToast("Lo sentimos, el servidor no responde", "long", "bottom");
        });
    }

    //JY, Mensaje Personalizado
    showToast(message, duration, position) {
        this.platform.ready().then(() => {
            window.plugins.toast.show(message, duration, position);
        });
    }
}